-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2022 at 01:31 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_22_p1`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_data`
--

CREATE TABLE `tb_data` (
  `data_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `suhu` varchar(100) NOT NULL,
  `lokasi` text NOT NULL,
  `jam` varchar(15) DEFAULT NULL,
  `tanggal` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_data`
--

INSERT INTO `tb_data` (`data_id`, `user_id`, `suhu`, `lokasi`, `jam`, `tanggal`) VALUES
(54, 1, '12.2', 'samarinda', '12:12', '2022-04-24'),
(55, 1, '32.2', 'india', '12:23', '2022-04-24'),
(56, 1, '37.2', 'pluto', '23:11', '2022-04-24'),
(57, 1, '22.2', 'kutub', '04:54', '2022-04-24'),
(58, 4, '99', 'gunung berapi', '12:31', '2022-04-26'),
(59, 6, '43', 'isekai', '12:12', '2022-04-26'),
(60, 11, '32.4', 'Kota Bekasi', '02:30', '2022-04-14'),
(65, 12, '44', 'mountain\r\n', '21:02', '2022-05-09'),
(66, 11, '99', 'rawa', '21:02', '0001-12-13'),
(67, 6, '55', 'bandige', '12:31', '2022-05-11'),
(68, 6, '888', 'muka robert', '00:32', '2022-05-05'),
(69, 12, '34', 'palaran', '12:34', '2022-05-06'),
(70, 12, '33', 'sumber', '17:09', '2022-05-11'),
(73, 1, '32', 'mujarab', '17:52', '2022-05-12'),
(74, 1, '32', 'adasda', '12:31', '2022-05-10'),
(75, 1, '33', 'bekasi', '12:33', '2022-05-04');

-- --------------------------------------------------------

--
-- Table structure for table `tb_users`
--

CREATE TABLE `tb_users` (
  `user_id` int(11) NOT NULL,
  `user_fullname` varchar(100) DEFAULT NULL,
  `nik` char(16) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_pass` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_users`
--

INSERT INTO `tb_users` (`user_id`, `user_fullname`, `nik`, `user_email`, `user_pass`) VALUES
(1, 'rickyazidane', '1111111111111111', 'ricky.azidane@student.smkti.net', 'bakemonogatari'),
(4, 'Rikii', '2222222222222222', 'Riki@gmail.com', '111111'),
(6, 'qwerty', '3333333333333333', 'qwerty@gmail.com', 'qwerty'),
(11, 'robert', '123', 'robert@gmail.com', '123'),
(12, 'dayat', '12345', 'dayat@gmail.com', '12345');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_data`
--
ALTER TABLE `tb_data`
  ADD PRIMARY KEY (`data_id`);

--
-- Indexes for table `tb_users`
--
ALTER TABLE `tb_users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_data`
--
ALTER TABLE `tb_data`
  MODIFY `data_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `tb_users`
--
ALTER TABLE `tb_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
    require_once("../config/Connect.php");
    require_once("../models/Data.php");

    $data = new Data();

    switch($_GET["op"]){
        case "insert" :
            $data->insert_data(       
            $_POST["suhu"],
            $_POST["lokasi"],
            $_POST["jam"],
            $_POST["user_id"],
            $_POST["tanggal"]);
        break;
        case "getData" :
                $data_hasil = $data->get_data($_POST["user_id"]);
                $data = array();
                foreach($data_hasil as $row){
                    $sub_array = array();
                    // $sub_array[] = $row["data_id"];
                    $sub_array[] = $row["suhu"];
                    $sub_array[] = $row["lokasi"];
                    $sub_array[] = $row["jam"];
                    $sub_array[] = $row["tanggal"];

                $data[] = $sub_array;
             }
                $result = array(
                    "sEcho" => 1,
                    "iTotalRecords" => count($data),
                    "iTotalDisplayRecords" =>count($data),
                    "aaData" => $data
                 );

                echo json_encode($result);
             break;

    }

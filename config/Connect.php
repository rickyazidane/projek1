<?php
session_start();
class Connect{
    protected $db;

    protected function Connection(){
        try{
            $connect = $this->db = new PDO("mysql:local=localhost;dbname=db_22_p1","root", "");
            return $connect;
        }catch(Exception $e){
            print "Error: " . $e->getMessage()."<br>";
            die();
        }
    }

    public function set_name(){
        return $this->db->query("SET NAMES 'utf8'");
    }

    public static function base_url(){
        return "http://localhost/ukk_22_p1_rickyazidane/";
    }
}
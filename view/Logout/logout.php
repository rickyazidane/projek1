<?php

    require_once("../../config/Connect.php");
    session_destroy();
    header("Location: ".Connect::base_url());
    
    exit();
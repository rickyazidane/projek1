<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">

      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
       
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">

      <li>
          <a href="..\Home\">
          <i class="fa fa-th"></i> <span>Home</span>
            </span>
          </a>
        </li>
        <li>
          <a href="..\IsiData\">
            <i class="fa fa-files-o"></i>
            <span>Isi Data</span>
            </span>
          </a>
        </li>
        <li>
          <a href="..\CatatanPerjalanan\">
            <i class="fa fa-bed"></i>
            <span>Catatan Perjalanan</span>
            </span>
          </a>
        </li>
</aside>
    <!-- jQuery 3 -->
<script src="../../public/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../public/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../public/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../public/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../public/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../public/js/demo.js"></script>
 
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>

<!-- editor -->
<script type="text/javascript" src="../../public/bower_components/ckeditor/lang/id.js?t=J5S9"></script>
<script type="text/javascript" src="../../public/bower_components/ckeditor/styles.js?t=J5S9"></script>
<script type="text/javascript" src="../../public/bower_components/ckeditor/config.js?t=J5S9"></script>
<script type="text/javascript" src="../../public/bower_components/ckeditor/ckeditor.js"></script>

<!-- sweet alert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- DataTables -->
<script src="../../public/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../public/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


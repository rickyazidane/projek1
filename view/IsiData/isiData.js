function init(){
    $("#data-form").on("submit", function(e){
        saveAndEdit(e);
    })
  }
  
  function saveAndEdit(e) {
    e.preventDefault();
    var formData = new FormData($("#data-form")[0]);
  
    if (
    $('#suhu').val() == '' || 
    $('#lokasi').val() == '' || 
    $('#jam').val() == '' || 
    $('#tanggal').val() == '') {
      
      swal("perhatian", "Silahkan isi data terlebih dahulu", "warning");
    } else {
      $.ajax({
        url: "../../controller/Data.php?op=insert",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
          $('#suhu').val('');
          $('#lokasi').val('');
          $('#jam').val('');
          $('#tanggal').val('');
          swal("Selamat", "Data baru berhasil Dimasukan", "success");
        }
      })
    }
  }
  
  init();
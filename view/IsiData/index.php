<?php
  session_start();
  if(isset($_SESSION["user_id"])){

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>Isi Data</title>
  
</head>
<body class="hold-transition skin-blue sidebar-mini">

    <!-- Link -->
    <?php require_once("../LayoutPartial/link.php"); ?>
    <!-- Header -->
    <?php require_once("../LayoutPartial/header.php"); ?>
    <!-- nav -->
    <?php require_once("../LayoutPartial/nav.php"); ?>


    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Halaman Tambah Data
            
        </h1>
        <ol class="breadcrumb">
            <li><a href="../Home/index.php"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#">Isi Data</a></li>
        </ol>
    </section>

<!-- Main content -->
<section class="content">

    <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Baru</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="POST" id="data-form" class="form-horizontal" autocomplete="off">
              <div class="box-body">
                <!-- Kategori Tiket -->

                <!-- <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Keadaan Tubuh</label>
                  <div class="col-sm-6">
                    <select id="cat_select" name="cat_id" class="form-control">
                    </select>
                  </div>
                </div> -->
                <!-- Suhu -->
                <input type="hidden" name="user_id" value="<?php echo $_SESSION["user_id"]?>">
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Suhu Tubuh</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="suhu" placeholder="Suhu" name="suhu">
                  </div>
                </div>
                <!-- jam -->
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jam</label>
                  <div class="col-sm-6">
                    <input type="time" class="form-control" id="jam" placeholder="jam" name="jam">
                  </div>
                </div>
                <!-- tanggal -->
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Tanggal</label>
                  <div class="col-sm-6">
                    <input type="date" class="form-control" id="tanggal" placeholder="tanggal" name="tanggal">
                  </div>
                </div>
                <!-- lokasi yang dikunjungi -->
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Lokasi yang dikunjungi</label>
                   <div class="col-sm-8">
                  <textarea class="form-control" placeholder="..." id="lokasi" name="lokasi"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
              </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="reset" class="btn btn-default">Batal</button>
                <button type="submit" name="action-save" value="save" class="btn btn-info">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
    <!-- /.box -->
</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- script -->
<?php require_once("../LayoutPartial/script.php"); ?>
<script src="isiData.js" type="text/javascript"></script>
</body>
</html>
<?php
  }else{
    header("Location: ".Connect::base_url()."index.php?m=2");
  }
?>x 
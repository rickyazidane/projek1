<?php   
    require_once("../../config/Connect.php");
    if(isset($_SESSION["user_id"])){
    ?>

<?php
$host       = "localhost";
$user       = "root";
$pass       = "";
$db         = "db_22_p1";

$koneksi    = mysqli_connect($host, $user, $pass, $db);
if (!$koneksi) { //cek koneksi
    die("Tidak bisa terkoneksi ke database");
}
$data_id     = "";
$suhu        = "";
$lokasi      = "";
$jam         = "";
$tanggal     = "";
$user_id     = "";
$sukses      = "";
$error       = "";

if (isset($_GET['op'])) {
    $op = $_GET['op'];
} else {
    $op = "";
}
if($op == 'delete'){
    $data_id    = $_GET['data_id'];
    $sql1       = "delete from tb_data where data_id = '$data_id'";
    $q1         = mysqli_query($koneksi,$sql1);
    if($q1){
        $sukses = "Berhasil hapus data";
    }else{
        $error  = "Gagal melakukan delete data";
    }
}
if ($op == 'edit') {
    $data_id    = $_GET['data_id'];
    $sql1       = "select * from tb_data where data_id = '$data_id'";
    $q1         = mysqli_query($koneksi, $sql1);
    $r1         = mysqli_fetch_array($q1);
    $suhu       = $r1['suhu'];
    $lokasi     = $r1['lokasi'];
    $jam        = $r1['jam'];
    $tanggal    = $r1['tanggal'];
    $user_id    = $r1['user_id'];

}
if (isset($_POST['simpan'])) { //untuk create
    $suhu         = $_POST['suhu'];
    $lokasi       = $_POST['lokasi'];
    $jam          = $_POST['jam'];
    $tanggal      = $_POST['tanggal'];

    if ($suhu && $lokasi && $jam && $tanggal) {
        if ($op == 'edit') { //untuk update
            $sql1       = "update tb_data set suhu = '$suhu',lokasi='$lokasi',jam = '$jam',tanggal='$tanggal' where data_id = '$data_id'";
            $q1         = mysqli_query($koneksi, $sql1);
            if ($q1) {
                $sukses = "Data berhasil diupdate";
            } else {
                $error  = "Data gagal diupdate";
            }
        } 
    } else {
        $error = "Silakan masukkan semua data";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Perjalanan</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <style>
        .mx-auto {
            width: 800px
        }

        .card {
            margin-top: 10px;
        }
    </style>
</head>

<body>
    
    <div class="mx-auto">
    <div class="card">
            <div class="card-header">
                Edit Data
            </div>
            <div class="card-body">
                <?php
                if ($error) {
                ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $error ?>
                    </div>
                <?php
                    header("refresh:5;url=edit.php");//5 : detik
                }
                ?>
                <?php
                if ($sukses) {
                ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $sukses ?>
                    </div>
                <?php
                    header("refresh:5;url=edit.php");
                }
                ?>
                <form action="" method="POST">
                    <div class="mb-3 row">
                        <label for="suhu" class="col-sm-2 col-form-label">suhu</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="suhu" name="suhu" value="<?php echo $suhu ?>">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="lokasi" class="col-sm-2 col-form-label">lokasi</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="lokasi" name="lokasi" value="<?php echo $lokasi ?>">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="jam" class="col-sm-2 col-form-label">jam</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="jam" name="jam" value="<?php echo $jam ?>">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="tanggal" class="col-sm-2 col-form-label">tanggal</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="tanggal" name="tanggal" value="<?php echo $tanggal ?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <input type="submit" name="simpan" value="Simpan Data" class="btn btn-primary" />
                    </div>
                </form>
            </div>
        </div>

        <!-- untuk mengeluarkan data -->
        <div class="card">
            <div class="card-header text-white bg-secondary">
                Data Perjalanan
                <a href="../CatatanPerjalanan/index.php"><i class="fa fa-home"></i> Back</a>
            </div>
            
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Suhu</th>
                            <th scope="col">Lokasi</th>
                            <th scope="col">Jam</th>
                            <th scope="col">Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $id_user = $_SESSION["user_id"];
                        $sql2   = "select * from tb_data WHERE user_id = '$id_user' ";
                        $q2     = mysqli_query($koneksi, $sql2);
                        $urut   = 1;
                        while ($r2 = mysqli_fetch_array($q2)) {
                            $data_id      = $r2['data_id'];
                            $suhu         = $r2['suhu'];
                            $lokasi       = $r2['lokasi'];
                            $jam          = $r2['jam'];
                            $tanggal      = $r2['tanggal'];
                        ?>
                            <tr>
                                <th scope="row"><?php echo $urut++ ?></th>
                                <td scope="row"><?php echo $suhu ?></td>
                                <td scope="row"><?php echo $lokasi ?></td>
                                <td scope="row"><?php echo $jam ?></td>
                                <td scope="row"><?php echo $tanggal ?></td>
                                <td scope="row">
                                    <a href="edit.php?op=edit&data_id=<?php echo $data_id ?>"><button type="button" class="btn btn-warning">Edit</button></a>
                                    <a href="edit.php?op=delete&data_id=<?php echo $data_id?>" onclick="return confirm('Yakin mau delete data?')"><button type="button" class="btn btn-danger">Delete</button></a>            
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>
</body>

</html>

<?php
    }else{
        header("Location: ".Connect::base_url()."index.php");
    }
    ?>

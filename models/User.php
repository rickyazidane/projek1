<?php
class User extends Connect{
    public function login(){
        $connect=parent::connection();
        parent::set_name();

        if(isset($_POST["submit"])){
            $email = $_POST["user_email"];
            $pass = $_POST["user_pass"];
            $nik = $_POST["nik"];

            if(empty($email) and empty($pass)){
                header("Location:".connect::base_url()."index.php?m=2");
                exit();
            }else{
                $sql = "SELECT * FROM tb_users WHERE user_email=? and 
                user_pass=? and nik=?";
                $stmt = $connect->prepare($sql);
                $stmt->bindValue(1, $email);
                $stmt->bindValue(2, $pass);
                $stmt->bindValue(3, $nik);
                $stmt->execute();
                $result = $stmt->fetch();

                if(is_array($result) and count($result)>0){
                    $_SESSION["user_id"]=$result["user_id"];
                    $_SESSION["user_fullname"]=$result["user_fullname"];
                    $_SESSION["user_email"]=$result["user_email"];
                    $_SESSION["nik"]=$result["nik"];
                   header ("Location:".connect::base_url()."view/Home/");
                   exit();
                }else{
                  header ("Location:".connect::base_url()."index.php?m=1");
                  exit();
            }
        }
    }

}

public function update_user($user_id, $role_id, $user_fullname, $user_email, $user_pass,$nik)
{
    $connect=parent::connection();
        parent::set_name();

        $sql = "UPDATE tb_users
        SET
        role_id = ?, user_fullname = ?,user_email = ?, user_pass = ?,nik = ?
        WHERE
        user_id = ? ";

        $sql = $connect->prepare($sql);
        $sql->bindValue(1, $user_id);
        $sql->bindValue(3, $user_fullname);
        $sql->bindValue(5, $user_email);
        $sql->bindValue(6, $user_pass);
        $sql->bindValue(7, $nik);
        $sql->execute();    
        return $result = $sql->fetchAll();
}

public function get_user($user_id)
{
    $connect = parent::connection();
    parent::set_names();

    $sql = "SELECT FROM tb_users WHERE user_id = ?";
    $sql = $connect->prepare($sql);
    $sql->bindValue(1, $user_id);
    $sql->execute();    
    return $result = $sql->fetchAll();
}


}
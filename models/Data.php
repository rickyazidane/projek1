<?php
    class Data extends Connect{

        public function insert_data($suhu, $lokasi, $jam, $tanggal, $user_id){

            $connect = parent::connection();
            parent::set_name();

            $sql = "INSERT INTO tb_data(
                data_id,
                suhu,
                lokasi,
                jam,
                user_id,
                tanggal) VALUES (NULL,?,?,?,?,?)";

            $sql = $connect->prepare($sql);

            $sql->bindValue(1, $suhu);
            $sql->bindValue(2, $lokasi);
            $sql->bindValue(3, $jam);
            $sql->bindValue(4, $tanggal);
            $sql->bindValue(5, $user_id);

            $sql->execute();

            return $result=$sql->fetchAll();
        }

        public function get_data($user_id)
        {
            $connect = parent::connection();
            parent::set_name();

            // SQL JOIN TABLE USER, data, CATEGORY
            $sql = "SELECT * from tb_data WHERE user_id=?";
            $sql = $connect->prepare($sql);
            $sql->bindValue(1, $user_id);
            $sql->execute();
            return $result = $sql->fetchAll();
        }
    }